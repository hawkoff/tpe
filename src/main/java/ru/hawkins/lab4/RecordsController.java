package ru.hawkins.lab4;

import ru.hawkins.lab4.exceptions.EmailExistsException;
import ru.hawkins.lab4.exceptions.EmailValidationException;
import ru.hawkins.lab4.exceptions.PhoneExistsException;
import ru.hawkins.lab4.exceptions.RecordNotExistsException;

import java.util.Scanner;

/**
 * Created by Egor on 27.03.2016.
 */
public class RecordsController {
    KeepService keepService;
    Scanner in;

    RecordsController() {
        keepService = new KeepService();
        in = new Scanner(System.in);
    }
    public int actionListener(String action) {
        switch (action) {
            case "show": {
                System.out.printf("%15s%15s%15s%n", "Имя", "E-mail", "Телефон");

                for (Record record: keepService.getAllRecords()) {
                    System.out.printf("%15s%15s%15s%n", record.getName(), record.getEmail(), record.getPhone());
                }
                return 1;
            }

            case "remove": {
                System.out.print("Имя для удаления: ");

                String name = in.next();

                try {
                    keepService.removeRecord(name);
                } catch (RecordNotExistsException e) {
                    System.out.println("Записи не существует");
                    return 2;
                }

                System.out.println("Запись успешно удалена");
                return 2;
            }

            case "add": {
                System.out.println("Добавление записии");
                System.out.print("Введите имя: ");
                String name = in.next();
                System.out.print("%nВведите e-mail: ");
                String email = in.next();
                System.out.print("%nВведите номер телефона: ");
                String phone = in.next();

                try {
                    keepService.addRecord(name, email, phone);
                } catch (EmailExistsException | EmailValidationException | PhoneExistsException e) {
                    System.out.println(e.getMessage());
                    return 3;
                }

                System.out.println("Запись успешно добавлена");
                return 3;
            }

            case "find": {
                System.out.print("Имя для поиска: ");

                String name = in.next();

                Record record = keepService.find(name);

                if (record == null) {
                    System.out.println("Запись не найдена");
                    return 4;
                }

                System.out.printf("%15s%15s%15s%n", "Имя", "E-mail", "Телефон");
                System.out.printf("%15s%15s%15s%n", record.getName(), record.getEmail(), record.getPhone());
                return 4;
            }

            default:
                System.out.println("Не поддерживаемая операция");
                return 0;
        }
    }

    public static void main(String[] args) {
        RecordsController controller= new RecordsController();

        Scanner in = new Scanner(System.in);

        String action = "";
        while (true) {
            System.out.println("Введите команду: ");
            action = in.next();
            if (action.equals("exit")) {
                break;
            }

            controller.actionListener(action);
        }
    }
}
