package ru.hawkins.lab4.exceptions;

/**
 * Created by Egor on 27.03.2016.
 */
public class EmailExistsException extends Exception {

    public EmailExistsException(String message) {
        super(message);
    }

    public EmailExistsException(String message, Throwable cause) {
        super(message, cause);
    }
}
