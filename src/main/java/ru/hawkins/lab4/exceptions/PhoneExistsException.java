package ru.hawkins.lab4.exceptions;

/**
 * Created by Egor on 27.03.2016.
 */
public class PhoneExistsException extends Exception {
    public PhoneExistsException(String message) {
        super(message);
    }

    public PhoneExistsException(String message, Throwable cause) {
        super(message, cause);
    }
}
