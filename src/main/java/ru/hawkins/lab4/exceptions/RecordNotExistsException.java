package ru.hawkins.lab4.exceptions;

/**
 * Created by Egor on 27.03.2016.
 */
public class RecordNotExistsException extends Exception {
    public RecordNotExistsException(String message) {
        super(message);
    }

    public RecordNotExistsException(String message, Throwable cause) {
        super(message, cause);
    }
}
