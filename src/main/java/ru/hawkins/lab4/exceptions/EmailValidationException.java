package ru.hawkins.lab4.exceptions;

/**
 * Created by Egor on 27.03.2016.
 */
public class EmailValidationException extends Exception {
    public EmailValidationException(String message) {
        super(message);
    }

    public EmailValidationException(String message, Throwable cause) {
        super(message, cause);
    }
}
