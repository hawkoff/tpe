package ru.hawkins.lab4;

import ru.hawkins.lab4.exceptions.EmailValidationException;
import ru.hawkins.lab4.exceptions.EmailExistsException;
import ru.hawkins.lab4.exceptions.PhoneExistsException;
import ru.hawkins.lab4.exceptions.RecordNotExistsException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by Egor on 20.03.2016.
 */
public class KeepService {
    // Для тестирования оставлен как protected
    protected List<Record> records;

    KeepService() {
        records = new ArrayList<>();
    }

    public void addRecord(String name, String email, String phone)
            throws EmailExistsException, EmailValidationException, PhoneExistsException {
        for (Record record : records) {
            if (record.getEmail().equalsIgnoreCase(email)) {
                throw new EmailExistsException("Такой емайл уже добавлен");
            }

            if (record.getPhone().equalsIgnoreCase(phone)) {
                throw new PhoneExistsException("Такой телефон уже существует");
            }

        }

        Pattern emailPattern = Pattern.compile("^[^@]*@[^@]*$");
        Matcher matcher = emailPattern.matcher(email);
        if (!matcher.find()) {
            throw new EmailValidationException("Емайл некорректный");
        }

        records.add(new Record(name, phone, email));
    }

    public Record find(String name) {
        for (Record r : records) {
            if (name.equalsIgnoreCase(r.getName())) {
                return r;
            }
        }

        return null;
    }

    public void removeRecord(String name) throws RecordNotExistsException {
        Record recordToDel = null;
        for (Record r : records) {
            if (name.equalsIgnoreCase(r.getName())) {
                recordToDel = r;
            }
        }

        if (recordToDel != null) {
            records.remove(recordToDel);
        } else {
            throw new RecordNotExistsException("Такой записи нет");
        }
    }

    public List<Record> getAllRecords() {
        return records.stream().collect(Collectors.toList());
    }

}
