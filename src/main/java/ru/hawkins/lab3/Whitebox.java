package ru.hawkins.lab3;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Egor on 27.03.2016.
 */
public class Whitebox {
    public Result findMaxAndMax2(List<Integer> list) {
        // Ужасно, так делать нельзя!

        int max1 = Integer.MIN_VALUE;
        int posMax1 = 0;

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) > max1) {
                max1 = list.get(i);
                posMax1 = i;
            }
        }

        int max2 = Integer.MIN_VALUE;

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) > max2 && list.get(i) != max1) {
                max2 = list.get(i);
            }
            if (max1 == list.get(i) && posMax1 != i) {
                max2 = list.get(i);
            }
        }

        return new Result(max1, max2);
//        List<Integer> sorted = list.stream()
//                .sorted(Comparator.reverseOrder())
//                .collect(Collectors.toList());
//        return new Result(sorted.get(0), sorted.get(1));
    }

    public static class Result {
        int max;

        int max2;

        public Result(int max, int max2) {
            this.max = max;
            this.max2 = max2;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Result result = (Result) o;

            if (max != result.max) return false;
            return max2 == result.max2;

        }

        @Override
        public int hashCode() {
            int result = max;
            result = 31 * result + max2;
            return result;
        }
    }
}
