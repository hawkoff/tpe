package ru.hawkins.lab3;

import org.junit.Before;
import org.junit.Test;
import ru.hawkins.lab3.Whitebox;

import java.lang.reflect.Array;
import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * Created by Egor on 27.03.2016.
 */
public class WhiteboxTest {

    Whitebox whitebox;

    @Before
    public void init() {
        whitebox = new Whitebox();
    }


    @Test
    public void testFindMaxAndMax2() throws Exception {
        assertEquals(whitebox.findMaxAndMax2(Arrays.asList(1, 2, 3, 15, 14, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13)),
                new Whitebox.Result(15, 14));
    }

    @Test
    public void should() {
        assertEquals(whitebox.findMaxAndMax2(Arrays.asList(15, 15, 3, 15, 14, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13)),
                new Whitebox.Result(15, 15));
    }
}