package ru.hawkins.lab4;

import org.junit.Before;
import org.junit.Test;
import ru.hawkins.lab4.exceptions.EmailExistsException;
import ru.hawkins.lab4.exceptions.EmailValidationException;
import ru.hawkins.lab4.exceptions.PhoneExistsException;
import ru.hawkins.lab4.exceptions.RecordNotExistsException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Egor on 27.03.2016.
 */
public class KeepServiceTest {

    KeepService keepService;

    @Before
    public void init() {
        keepService = new KeepService();
    }

    @Test(expected = EmailValidationException.class)
    public void shouldThrowEmailValidationException_addRecord()
            throws PhoneExistsException, EmailValidationException, EmailExistsException {
        keepService.addRecord("some name", "not_valid_email", "135");
    }

    @Test(expected = EmailExistsException.class)
    public void shouldThrowEmailExistsException_addRecord()
            throws PhoneExistsException, EmailValidationException, EmailExistsException {
        keepService.records = Arrays.asList(new Record("name", "a@b", "123"));

        keepService.addRecord("some name", "a@b", "123");
    }

    @Test(expected = PhoneExistsException.class)
    public void shouldThrowPhoneExistsException_addRecord()
            throws PhoneExistsException, EmailValidationException, EmailExistsException {
        keepService.records = Arrays.asList(new Record("name", "a@b", "123"));

        keepService.addRecord("some name", "name@host", "123");
    }

    @Test
    public void shouldSuccessfullyAddRecord_addRecord()
            throws PhoneExistsException, EmailValidationException, EmailExistsException {
        keepService.addRecord("name", "name@host", "123");

        assertEquals(keepService.records.size(), 1);
    }

    @Test
    public void shouldFindCorrectRecord_find() {
        Record r = new Record("name", "a@b", "123");
        keepService.records = Arrays.asList(r, new Record("name2", "name@host", "1234"));

        assertEquals(keepService.find("name"), r);
    }

    @Test
    public void shouldNotFindRecord_find() {
        Record r = new Record("name", "a@b", "123");
        keepService.records = Arrays.asList(r, new Record("name2", "name@host", "1234"));

        assertNull(keepService.find("name3"));
    }

    @Test(expected = RecordNotExistsException.class)
    public void shouldThrowRecordNotExistsException_removeRecord() throws Exception {
        keepService.removeRecord("name");
    }

    @Test
    public void shouldThrowSuccessfullyRemovedRecord_removeRecord() throws RecordNotExistsException {
        List<Record> records = new ArrayList<>();
        records.add(new Record("name", "a@b", "123"));
        records.add(new Record("name2", "name@host", "1234"));

        keepService.records = records;

        keepService.removeRecord("name");

        assertEquals(keepService.records.size(), 1);
    }

    @Test
    public void shouldGetAllList() {
        List<Record> records = new ArrayList<>();
        records.add(new Record("name", "a@b", "123"));
        records.add(new Record("name2", "name@host", "1234"));

        keepService.records = records;

        List<Record> returned = keepService.getAllRecords();
        assertEquals(returned, records);
    }
}