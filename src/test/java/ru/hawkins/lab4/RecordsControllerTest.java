package ru.hawkins.lab4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Scanner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;

/**
 * Created by Egor on 27.03.2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class RecordsControllerTest {

    @Mock
    KeepService keepService;

    @InjectMocks
    RecordsController recordsController = new RecordsController();

    @Test
    public void shouldExecuteShowAllRecord_actionListener() throws Exception {
        recordsController.actionListener("show");

        verify(keepService).getAllRecords();
    }

    @Test
    public void shouldNotExistsAction_actionListener() {
        assertEquals(recordsController.actionListener("sacgf"), 0);
    }
//
//    @Test
//    public void shouldExecuteRemove_actionListener() {
//        assertEquals(recordsController.actionListener("remove"), 2);
//    }
//
//    @Test
//    public void shouldNotExistsAdd_actionListener() {
//        assertEquals(recordsController.actionListener("add"), 3);
//    }
//
//    @Test
//    public void shouldNotExistsFind_actionListener() {
//        assertEquals(recordsController.actionListener("find"), 4);
//    }


}