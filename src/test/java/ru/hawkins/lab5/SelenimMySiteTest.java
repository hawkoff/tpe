package ru.hawkins.lab5;

import com.thoughtworks.selenium.Wait;
import com.thoughtworks.selenium.webdriven.commands.WaitForPageToLoad;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.List;

import static org.junit.Assert.*;

/**
 * TODO: можно что-то еще придумать с PageObject https://habrahabr.ru/post/236561/
 *
 * Created by Egor on 03.04.2016.
 */
public class SelenimMySiteTest {

    static WebDriver webDriver;

    @BeforeClass
    public static void init() {
        webDriver = new FirefoxDriver();
    }

    @Test
    public void shouldBeOnePostOnTheMAinPage() {
        webDriver.get("http://gor-hawkins.github.io");

        List<WebElement> elements = webDriver.findElements(By.xpath("//article"));

        assertEquals(elements.size(), 1);
    }

    @Test
    public void testTitleMainPage() {
        webDriver.get("http://gor-hawkins.github.io");

        assertEquals(webDriver.getTitle(), "Перспективы HTML5 и CSS3");
    }

    @Test
    public void shouldBeGoToAwesomeLink() throws InterruptedException {
        webDriver.get("http://gor-hawkins.github.io");

        WebElement awesomeLink = webDriver.findElement(
                By.xpath("//a[@href='http://siliconrus.com/2013/07/html-5-budushhee-interneta/']"));

        awesomeLink.click();

        Thread.sleep(100);

        assertEquals(webDriver.getTitle(), "HTML 5 — будущее интернета?");
    }

    @Test
    public void shouldFindSomeWord() throws InterruptedException {
        webDriver.get("http://gor-hawkins.github.io");

        WebElement searchBox = webDriver.findElement(By.id("s"));

        searchBox.sendKeys("Схема истории развития");

        Thread.sleep(1000);

        searchBox.submit();

        Thread.sleep(100);

        assertTrue(webDriver.findElement(By.id("content")).getText().contains("истории развития"));
    }

    @Test
    public void shouldChangePages() {
        webDriver.get("http://gor-hawkins.github.io");

        WebElement menuItem = webDriver.findElement(By.linkText("Новости"));

        menuItem.click();

        WebElement article = new WebDriverWait(webDriver, 100)
                .until((ExpectedCondition<WebElement>) input -> input.findElement(By.className("video")));

        assertTrue(article.isDisplayed());
    }

    @Test
    public void makeUIDesignScreenshot() throws IOException {
        webDriver.get("http://gor-hawkins.github.io");

        TakesScreenshot screenshotter = (TakesScreenshot) new Augmenter().augment(webDriver);

        File file = screenshotter.getScreenshotAs(OutputType.FILE);

        Files.move(file.toPath(), new File("screenshot.jpg").toPath(), StandardCopyOption.REPLACE_EXISTING);

    }

    @Test
    public void countMainMenuItemsByCSSSelector() {
        webDriver.get("http://gor-hawkins.github.io");

        List<WebElement> menuItems = webDriver.findElements(By.cssSelector("#main-nav li"));

        assertEquals(menuItems.size(), 4);
    }

    @Test
    public void checkTemplate() {
        webDriver.get("http://gor-hawkins.github.io/aspects.html");

        assertTrue(webDriver.findElement(By.tagName("aside")).isDisplayed());
        assertTrue(webDriver.findElement(By.tagName("header")).isDisplayed());
        assertTrue(webDriver.findElement(By.tagName("footer")).isDisplayed());
    }

    @Test
    public void checkLogoColor() {
        webDriver.get("http://gor-hawkins.github.io/aspects.html");

        assertEquals(webDriver.findElement(By.cssSelector("#site-logo a #html")).getCssValue("color"),
                "rgba(253, 115, 6, 1)");

        assertEquals(webDriver.findElement(By.id("css")).getCssValue("color"),
                "rgba(2, 119, 224, 1)");
    }

    @Test
    public void checkArticleTitleFont() {
        webDriver.get("http://gor-hawkins.github.io/aspects.html");

        WebElement element = webDriver.findElement(By.className("post-title"));

        assertTrue(element.getCssValue("font").contains("26px"));
    }

    @Test
    public void shouldChangeTitleOnChangePage() throws InterruptedException {
        String addr1 = "http://gor-hawkins.github.io/aspects.html";
        webDriver.get(addr1);

        String title1 = webDriver.getTitle();

        webDriver.findElement(By.linkText("Примеры")).click();

        Thread.sleep(1000);

        assertNotEquals("Адреса должны быть разными", addr1, webDriver.getCurrentUrl());
        assertEquals(title1, webDriver.getTitle());
    }

    @Test
    public void shouldLinkAndReturn() throws InterruptedException {
        String addr1 = "http://gor-hawkins.github.io/aspects.html";
        webDriver.get(addr1);

        String title1 = webDriver.getTitle();

        webDriver.findElement(By.linkText("Примеры")).click();

        Thread.sleep(1000);

        webDriver.findElement(By.linkText("Перспективы")).click();
        Thread.sleep(1000);

        assertEquals("Адреса должны быть равными", addr1, webDriver.getCurrentUrl());
        assertEquals(title1, webDriver.getTitle());
    }
}